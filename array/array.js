var students =[
    {name: "Biten Chandra Das", qualification: "I.T Engineer", start: 2015, end: 2019},
    {name: "Joydip Bonik", qualification: "Programmer", start: 2013, end: 2016},
    {name: "Hamid Hossen", qualification: "Engineer", start: 2010, end: 2016},
    {name: "Moktar Hossian", qualification: "Computer Technology", start: 2011, end: 2015},
    {name: "Probir", qualification: "B.Sc Engineer", start: 2014, end: 2017},
    {name: "Sukanta Dev", qualification: "I.T Engineer", start: 2014, end: 2018},
    {name: "Subrato", qualification: "Engineer", start: 2008, end: 2013}
];

const  ages =[22,25,18,20,21,24,22,17,26,30,33];
//
// console.log(students[0]);
// console.log(students[1]);
// console.log(students[2]);


for( let i = 0; i< students.length; i++){
    console.log(students[i]);

}

students.forEach(function (student) {
    console.log(student.name);
});



// let canDrink =[];
// for(let  i=0; i <ages.length; i++){
//
//     if(ages[i] >= 21){
//         canDrink.push(ages[i]);
//     }
// }


//
// const canDrink =ages.filter(function (age) {
//     if( age >= 21){
//         return true;
//     }
//
// });
//
// console.log(canDrink);

const  canDrink = ages.filter(age => age >= 21);
console.log(canDrink);

//filter retail

const engineerStudents = students.filter(student => student.qualification === 'Engineer');

console.log(engineerStudents);


const yearStart = students.filter(student => (student.start >=2013 && student.start < 2016));

console.log(yearStart);


// map
// Create array of students name
const studentsName = students.map(function (student) {
    return student.name;
    
});
console.log(studentsName);
const test = students.map(function (student) {
    return 1;

});
console.log(test);

// const testMap = students.map(function (student) {
//     return `${student.name}[${student.start} - ${student.end}]` ;
//
// });



const testMapArrow = students.map(student => `${student.name}[${student.start} - ${student.end}]`);
console.log(testMapArrow);
