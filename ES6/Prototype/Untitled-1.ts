var person = {
    kind: 'person'
  }
  
  var zack1 = {
    kind: 'person'
  }
  
  // creates a new object which prototype is person
  var zack = Object.create(person);

  var zack = Object.create(zack1, {age: {value:  13} }, {name: {value:  "Jon"} }, );
  var zack = Object.create(person, {name: {value:  "Biten Das"} }, );
    
  console.log(zack.kind);
  console.log(zack.age);
  console.log(zack.name);